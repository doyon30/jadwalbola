package com.doyonstudio.jadwalbola.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.doyonstudio.jadwalbola.R
import com.doyonstudio.jadwalbola.adapter.TabPagerAdapter
import com.doyonstudio.jadwalbola.fragment.NextMatchFragment
import com.doyonstudio.jadwalbola.fragment.LastMatchFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tabAdapter = TabPagerAdapter(supportFragmentManager)
        tabAdapter.addFragment(LastMatchFragment(), "Last Match")
        tabAdapter.addFragment(NextMatchFragment(), "Next Match")

        pagerMatch.adapter = tabAdapter
        tabMatch.setupWithViewPager(pagerMatch)
    }
}
