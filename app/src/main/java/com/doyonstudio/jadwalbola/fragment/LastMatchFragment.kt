package com.doyonstudio.jadwalbola.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.doyonstudio.jadwalbola.R
import com.doyonstudio.jadwalbola.adapter.LastMatchAdapter
import com.doyonstudio.jadwalbola.data.LastMatch
import com.doyonstudio.jadwalbola.invisible
import com.doyonstudio.jadwalbola.presenter.MainPresenter
import com.doyonstudio.jadwalbola.view.MainView
import com.doyonstudio.jadwalbola.visisble
import kotlinx.android.synthetic.main.fragment_last_match.view.*
import org.jetbrains.anko.support.v4.ctx

class LastMatchFragment : Fragment(), MainView {

    private lateinit var progressBar: ProgressBar
    private val items: MutableList<LastMatch> = mutableListOf()
    private lateinit var adapter: LastMatchAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_last_match, container, false)

        progressBar = v.lastMatchProgressBar
        adapter = LastMatchAdapter(items)

        val recy = v.lastMatchRecy
        recy.layoutManager = LinearLayoutManager(ctx)
        recy.adapter = adapter

        MainPresenter(this).getLastMatch()

        return v
    }

    override fun showLoading() {
        progressBar.visisble()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showLastMatch(lastMatch: List<LastMatch>) {
        items.clear()
        items.addAll(lastMatch)
        adapter.notifyDataSetChanged()
    }

}
