package com.doyonstudio.jadwalbola.view

import com.doyonstudio.jadwalbola.data.LastMatch

interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showLastMatch(lastMatch: List<LastMatch>)
}