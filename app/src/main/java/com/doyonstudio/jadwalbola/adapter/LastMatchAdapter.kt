package com.doyonstudio.jadwalbola.adapter

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.doyonstudio.jadwalbola.R
import com.doyonstudio.jadwalbola.data.LastMatch
import kotlinx.android.synthetic.main.row_match_recy.view.*
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

class LastMatchAdapter(private val lastMatch: List<LastMatch>): RecyclerView.Adapter<LastMatchAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_match_recy, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int = lastMatch.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(lastMatch[position])
    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v)  {

        val homeTeam: TextView = v.homeTeam
        val homeScore: TextView = v.homeScore
        val awayTeam: TextView = v.awayTeam
        val awayScore: TextView = v.awayScore
        val dateMatch: TextView = v.dateMatch

        @SuppressLint("SimpleDateFormat")
        fun bindItem(lm: LastMatch){
            homeTeam.text = lm.homeTeam
            homeScore.text = lm.homeScore
            awayTeam.text = lm.awayTeam
            awayScore.text = lm.awayScore

            // Ubah format tanggal match
            val oldDateFormat = SimpleDateFormat("yyyy-MM-dd")
            val oldDate = oldDateFormat.parse(lm.dateMatch)

            val newDateFormat = SimpleDateFormat("EEE, dd MMM yyyy")
            val newDate = newDateFormat.format(oldDate)

            dateMatch.text = newDate
        }
    }

}