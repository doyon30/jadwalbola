package com.doyonstudio.jadwalbola.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class TabPagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm){

    val fragment: ArrayList<Fragment> = ArrayList()
    val fragTitle: ArrayList<String> = ArrayList()

    override fun getItem(position: Int): Fragment {
        return fragment[position]
    }

    override fun getCount(): Int {
        return fragment.size
    }

    fun addFragment(frag: Fragment, title: String){
        fragment.add(frag)
        fragTitle.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragTitle[position]
    }
}